import logo from "./logo.svg";
import "./App.css";
import Navbarr from "./component/Navbarr";
import Home from "./component/Home";
import { Switch, Route } from "react-router-dom";
import View from "./component/View";
import { BrowserRouter as Router } from "react-router-dom";
import Category from "./component/Category";
import NewArticle from "./component/NewArticle";

import React, { Component } from "react";
import strings from "./localization/languages";

export default class App extends Component {
  changeLanguage = (s) => {
    strings.setLanguage(s);
    this.setState({});
  };
  render() {
    return (
      <div>
        <Router>
          <Navbarr changeLanguage={this.changeLanguage} />
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route path="/view/:id">
              <View />
            </Route>
            <Route path="/category">
              <Category />
            </Route>
            <Route path="/addArticle">
              <NewArticle />
            </Route>
          </Switch>
        </Router>
      </div>
    );
  }
}
