import * as actions from "../actions/actionType";

const defaultState = {
  category: [],
};

export const categoryReducer = (state = defaultState, action) => {
  switch (action.type) {
    case actions.GET_CATEGORY:
      return { category: action.data };
    case actions.DELETE_CATEGORY:
      return {
        category: state.category.filter((cat) => cat._id !== action.data),
      };
    default:
      return state;
  }
};
