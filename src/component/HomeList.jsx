import React, { Component } from "react";
import "./main.css";
import { Link } from "react-router-dom";
import Btn from "./Btn";
import strings from "../localization/languages";

export default class HomeList extends Component {
  constructor() {
    super();
    this.substring = this.substring.bind(this);
  }
  substring = (art) => {
    let year = art.substring(0, 4);
    let month = art.substring(5, 7);
    let day = art.substring(8, 10);
    let date = day + "/" + month + "/" + year;
    let Date = [year, month, day];
    return Date.join("-");
  };

  render(props) {
    const btn = <Btn data={this.props.data} />;
    return (
      <div className="container listContainer" key={this.props.data._id}>
        <div className="row">
          <div className="col-4">
            <img
              src={
                this.props.data.image === undefined ||
                this.props.data.image === "string" ||
                this.props.data.image === null
                  ? "https://image.shutterstock.com/image-vector/ui-image-placeholder-wireframes-apps-260nw-1037719204.jpg"
                  : this.props.data.image
              }
              alt="erroe"
              width="100%"
            />
          </div>
          <div className="col-8">
            <h2>{this.props.data.title}</h2>
            <p className="nomp smaller">
              {strings.createDate} : {this.substring(this.props.data.createdAt)}{" "}
            </p>
            <p className="smaller">
              {strings.category}:{" "}
              {this.props.data.category === undefined ||
              this.props.data.category === null
                ? "no-type"
                : this.props.data.category.name}
            </p>
            <p>{this.props.data.description}</p>
            {this.props.isView === false ? btn : null}
          </div>
        </div>
      </div>
    );
  }
}
