import React from "react";

function RadioButton(props) {
  return (
    <div className="custom-control custom-radio d-inline mx-2">
      <input
        type="radio"
        id={props.category._id}
        name="category"
        className="custom-control-input"
        value={props.category._id}
        onChange={props.handleRadio}
        checked={props.categoryId === props.category._id}
      />
      <label className="custom-control-label" htmlFor={props.category._id}>
        {props.category.name}
      </label>
    </div>
  );
}

export default RadioButton;
