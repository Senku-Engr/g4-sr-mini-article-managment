import React, { Component } from "react";
import { withRouter } from "react-router";
import { bindActionCreators } from "redux";
import RadioButton from "./RadioButton";
import { getCategory } from "../redux/actions/categoryAction";
import { Form, Button } from "react-bootstrap";
import {
  uploadPicture,
  getArticleByIdNoAwait,
  putArticleNoAwait,
  postArticleNoAwait,
  postArticle,
} from "../redux/actions/articleAction";
import queryString from "query-string";
import { connect } from "react-redux";
import strings from "../localization/languages";

class NewArticle extends Component {
  constructor() {
    super();
    this.state = {
      title: "",
      categoryId: 0,
      updateId: "",
      description: "",
      noTitle: "",
      noDescription: "",
      isUpdate: false,
      pictureFile: "",
      pictureLink: "",
    };
    this.handleChangePicture = this.handleChangePicture.bind(this);
  }

  validate = () => {
    let noDescriptionErr = "";
    let noTitleErr = "";
    if (this.state.title === "") {
      noTitleErr = "* Title can not be blank";
    }
    if (this.state.description === "") {
      noDescriptionErr = "* Description can not be blank";
    }
    if (noTitleErr !== "" || noDescriptionErr !== "") {
      this.setState({
        noTitle: noTitleErr,
        noDescription: noDescriptionErr,
      });
      return false;
    }
    return true;
  };

  newArticle = () => {
    if (this.state.pictureFile) {
      let file = new FormData();
      file.append("image", this.state.pictureFile);
      uploadPicture(file, (res) => {
        let article = {
          title: this.state.title,
          description: this.state.description,
          category: this.state.categoryId,
          image: res,
        };
        postArticleNoAwait(article, (msg) => {
          alert(msg);
          this.props.history.push("/");
        });
      });
    } else {
      let article = {
        title: this.state.title,
        description: this.state.description,
        category: this.state.categoryId,
      };
      postArticleNoAwait(article, (msg) => {
        alert(msg);
        this.props.history.push("/");
      });
    }
  };

  updateArticle = () => {
    if (this.state.pictureFile) {
      let file = new FormData();
      file.append("image", this.state.pictureFile);
      uploadPicture(file, (res) => {
        let article = {
          title: this.state.title,
          description: this.state.description,
          category: this.state.categoryId,
          image: res,
        };
        putArticleNoAwait(article, this.state.updateId, (message) => {
          alert(message);
          this.props.history.push("/");
        });
      });
    } else {
      let article = {
        title: this.state.title,
        description: this.state.description,
        category: this.state.categoryId,
        image: this.state.pictureLink,
      };
      putArticleNoAwait(article, this.state.updateId, (message) => {
        alert(message);
        this.props.history.push("/");
      });
    }
  };

  handleSubmit = () => {
    let isValid = this.validate();
    if (isValid) {
      this.state.isUpdate ? this.updateArticle() : this.newArticle();
    }
  };
  handleRadioButton = (e) => {
    this.setState({
      categoryId: e.target.value,
    });
  };

  handleChange = (e) => {
    let name = e.target.name;
    let value = e.target.value;
    this.setState({
      [name]: value,
    });
  };
  handleChangePicture = (e) => {
    this.setState({
      pictureFile: e.target.files[0],
      pictureLink: URL.createObjectURL(e.target.files[0]),
    });
  };

  componentWillMount() {
    this.props.getCategory();
    let path = queryString.parse(this.props.location.search);
    if (path.id !== undefined || path.id !== undefined) {
      getArticleByIdNoAwait(path.id, (data) => {
        this.setState({
          isUpdate: Boolean(path.update),
          pictureLink: data.image,
          updateId: path.id,
          title: data.title,
          description: data.description,
          categoryId: data.category ? data.category._id : null,
        });
      });
    }
  }
  render() {
    let categories = this.props.category.map((cat) => (
      <RadioButton
        key={cat._id}
        category={cat}
        handleRadio={this.handleRadioButton}
        categoryId={this.state.categoryId}
      />
    ));

    return (
      <div className="container">
        <h1 className="my-4">
          {this.state.isUpdate ? strings.update : strings.add}
        </h1>
        <div className="row">
          <div className="col-md-7">
            <Form>
              <Form.Group controlId="title">
                <Form.Label>{strings.title}</Form.Label>
                <Form.Control
                  type="text"
                  placeholder={"Input title"}
                  value={this.state.title}
                  name="title"
                  onChange={this.handleChange.bind(this)}
                />
                <Form.Text className="text-danger">
                  {this.state.noTitle}
                </Form.Text>
              </Form.Group>
              <Form.Group>
                <Form.Label className="mr-3">{strings.category}</Form.Label>
                {categories}
              </Form.Group>
              <Form.Group controlId="description">
                <Form.Label>{strings.description}</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Input Description"
                  value={this.state.description}
                  name="description"
                  onChange={this.handleChange.bind(this)}
                />
                <Form.Text className="text-danger">
                  {this.state.noDescription}
                </Form.Text>
              </Form.Group>
              <Form.Label>{strings.thumbnail}</Form.Label>
              <Form.File
                id="custom-file-translate-scss"
                label="Custom file input"
                lang="en"
                onChange={this.handleChangePicture}
                custom
              />
              <Button
                variant="primary"
                className="my-3"
                onClick={() => this.handleSubmit()}
              >
                {this.state.isUpdate ? "Save" : "Submit"}
              </Button>
            </Form>
          </div>
          <div className="col-md-5">
            <img
              src={
                this.state.pictureLink === "" ||
                this.state.pictureLink === undefined
                  ? "https://image.shutterstock.com/image-vector/ui-image-placeholder-wireframes-apps-260nw-1037719204.jpg"
                  : this.state.pictureLink
              }
              alt="img"
              className="img-fluid"
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    category: state.categoryReducer.category,
  };
};

const mapDispatchToprops = (dispatch) => {
  return bindActionCreators(
    {
      getArticleByIdNoAwait,
      putArticleNoAwait,
      postArticleNoAwait,
      uploadPicture,
      getCategory,
    },
    dispatch
  );
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToprops)(NewArticle)
);
